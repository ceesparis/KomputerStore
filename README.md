# Komputer Store

This application offers a digital environment in which you can:

1. work
2. put your money on the bank
3. take out loans
4. buy laptops

Have fun!


## Usage

You can work buy clicking the work button. You have to put your money in the bank before you can spend it by clicking the bank button. You can take up a loan up to twice your savings by clicking the get a loan button. Select a laptop you would like using the dropdown menu and work until you can acquire it. Keep on going until you get all the laptops!

## Contributing

I have used bootstrap for styling! Got the asignment and some usefull videos from Noroff School of Technology and Digital Media.

## License

MIT © Cees Paris