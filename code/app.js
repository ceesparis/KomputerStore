// global variables
const IMAGE_URL = "https://noroff-komputer-store-api.herokuapp.com/"
const BACK_UP_IMAGE = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMREhUSEBISFQ8QFRYVFxYVFxUVEBcVFRUWFhUWFRUYHSggGBolGxYVITEhJikrLi4uFx8zODMsNygtLisBCgoKDg0OFRAQFS0dFR0tKy0rKy0rLS0tKystLS0tKystLS0tLS0rLS0rLS0tLTctKzc3Ny0tNystNys3LS0rK//AABEIALUBFwMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAAAwIEBQYHAQj/xABIEAABAwIDBAUJBAQNBQAAAAABAAIDBBESITEFBkFRBxMiYXEyQlKBkaGxwdEjkpPSFFSC4hUkMzRDRGJylKKj0+EWU2PC8P/EABcBAQEBAQAAAAAAAAAAAAAAAAABAgP/xAAaEQEBAQEBAQEAAAAAAAAAAAAAAREhEgIx/9oADAMBAAIRAxEAPwDuKIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAqJZA0FziGtaLkk2AA4knRVE210XGukLeeSpcWROtTMOQz7ZHnuAzIvpyRZNZDfXpKd2oqElrRe81rvd/cByYP7Rz5ALTd1OkSrpJftHunp3OOKORxc7M5uZIcw7jbQ3OQ1GobSmkIIxNF9O8/G6haSxzr6YjbgNfgi4+rdh7ZhrIhNTvDmHUaOaeLXjgQsgvnDc/bstHKJYTkbB7D5D28nfI8PavoXZlcyoiZNH5EjQ4cxzB7wbj1IlmLpEREEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBEUdRMGNc93ksaXHwAuUGr7+bdEUZgYftZR2reazjflfTwv3LkW1gAO1m4520FuFzwCy28W1QwPlqDeeocXYL5ht+y0+iLW9i1GOrdUBxfmXGzWt1093BHSTGH2tUWuBhxg3PZFvDPVUwVDiL3ub2ytx8FZVuIuOKxINsu7K/wAFTESNNVNG47LaDa+uVl1zox2l2XUzjoS5n/sB8fauJbKr+rLRJ7tRyXRNlzOgkZM3gQe4jiPktQsdmRUQSh7Wub5LgCPAi6rUcxERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAWq9Im2zTUxbG0vnn7LGgXPe49wy1yzW1LgPS5vw01b4I2vd1H2ZvdjA4ai1ru8dM0WMTLswvf1lXI0udo3EMLeNznmfasZVbVjaTHCDhzGK9hbw4DJYKs3gkfn1bQABzv7Vjv4Ufe4DeWmSjVq+nlDnFwyv7PUvYzodVjpNpSuNy73NA9wUf6bJ6RTE9NhiiLnA3IueGq6fsl3ZYx4vdtjxtoASuIs2hK3R7gsvQb41cRuJMVvSAKsX1H1LufMTBgOsRt+yc2n4j1LOrkHRH0htrJ/0aWPBUPYbFv8AJvLO16jbEuvozREREEREBERAREQEREBERAREQEREBERAREQEREBERAVL3gC5IAGpOQWK3p3ih2fTuqKgnC3JrR5b3HRrRzK5vtjfSSfCw2a90Jnwasa0AEgDLG4XtiJ1BsDnYN625vpT0zXlp617Glxa0gad5XF+kavh2jgqmdXHUluQYCesY3Itldwc2xs71eGn1lc98pkBLXE3uNfgreTaMrW2a+2bidTiLha5vxRUBi1HHvWJcLZHULdKiOOajp5gSKouMNr3D+raLlxPnEm4PeQtY2hTkEmxBGThxBHMKLerFERVkUkbVGtt6P8Ad1tZUsbNlADcjQvt5gPAcz9cpWvmNp6G9nwwTtrqtzmlgIgaMr4mlpkfzbYkAevkvoajrI5m44ntc08Qb+3kvnek3jdDXTBhIonvDQ1ruzG4ZYms0AxYr5W0yW6y71GhDZXWwvdhuAWEk6XDQWuJ8GqxbHWkWI2HtttQ0XGF/I2z8LErLowIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICEouT9Ou+MlNE2jpiRLUgiRzfKDCLYWkaE/DxQaz0z7YNTUsja68ETMTLHsuxef35aLTKfaZ62nkJOKEMhcSb4oySw3/AGHe0LL9IbAJIns8lrMA8IyRb2LUZDa9vJeDbxso1U1TDZzm8Wkj2GytZhcK6L75lQDUj1oi1le5rcAJsHYx3O5j3LaqjBV0zJ2gNmb2JbDU21I4j69y1x4v4he7LrzD1sfCVo9Tmm7T8UWMbVUzo3FrreIzaRzB5KFZauaXvdcjIm2Xqy9iqoKIA4zhwtzOIX9VjxTTEVBssuYZX9mIG1/OeeTProO/Rbp0aVdq1g0A0HAAaAf/AGa1+uqzIAT5N3WHDIN+qn3PqRHVMc42aL3PLjdRpZbajwTytBybNIB6nuHyWd27tc1BpYQbtiwX5OkdbP1DL1lYDaj+sc9589znfeJPzU2wn/bw38xwcf2e18kV1DY23ZDPUtiN4aSINHJ9Q1xL8J/y+q66nu1vBHVM8odaywc2/auQHD12IXAthbTbFHE247YM8rr2N3Em1/FZvdapdUGqqBIGMlGBo85lgercbDXzrd61rNjvqLVOjfec19LeXKqp3GGdvKRvH1/VbWjAiIgIiICIiAiIgIiICIiAiIgIiICIiC3r6tsMbpHkBrBfM28Avm3pJq3VNSZwXAtIwnQgjQgcMwutdIW2WOeyna9v2ZLn9oZOtlcdwz9fcuQb7VULS2MSXeQHWGliTbEdL9yNT8eRQCejYyWeCOoFngSOecTXC4cSxjsBIN8Ouml1jxuw6xaKyhsdO3Nl/oqF9W5haGnsmKI/6bR8l6NpP5/BTU1dt3Xda36XQfiTf7KN3Sfe/wCmUGn/AHJv9pW7dpv9L4KZm1ZPS+CgrO58h/rdB+LL/tKJu4kpcCKug1H9K/49WpxteT0vcFU3a8npe4J1U8+4M7nOc2p2fhLiR9ub2Jy8zVTT7hzkBrJ6C2p/jB14eYo2bwzAWxe4fRVs3im9L3D6KdXXknR/U4QOvoMiT/OBxDR6PcoYtwKoH+Wof8S36KZ23pj53uH0XjdrSel8FOmqpdwakjKWi/xMahg3DrGEkSUWLC4D+Mx5EtIv71Kdry+l8FQ7acvpH3J01G7cSrA8ujOVv51F9Vue4WzXUbSyoFO4PJLrTwOGeVrYs/8AlagNpyel8EG1ZPS+H0U2xZXR+hXYstGanr3R/bOxNDZWSuI4udhJ5jPvXVlw/czakretlAL+qjzaBmWkjFYDjYLp+7m3BI50D79YzySRbEwi4I9S383Yn1GwoiLTAiIgIiICIiAiIgIiICIiAiIgLw92q9XMelLfowufQ0r2skDL1E5NmQNeOy3mZHA5NGeY77Bwzb1BMJ5nNn64mSTE4EhxOM4jyNzfRYR8zrnHmdDi1yXTd1N3mvYJe0INGOkFnzc3NjHZjjy4hxOeYU21t2uuuXmNjRfC0UwyaOJk6wO4dyNY0OodlF3wM+Lh8lQHq73houolZHcnDCyxLSwkF8h8kk5etY8FRFy16rD1agqRrkFyHqoPVuHKvEoJ+sUjHq0a5TAoLtpVYKtY3qdr0EwS6oa5MSivQUJVGJUOcsVY3zcbbdLSteKmTA+YgR5G5wDtWI/vhZ3Zm8NOZ+rhqYjJ5jXXZIHa2GIcRmAtNot26yopmS0ractBeD1riH37OjT2CMhmQlbUVMOFu2KDrYo7YaiIAyxYSCC2aPS2RsbaaFdJeNO+bH2iJmXyxjJwBB9YtwV+vma82zqylrKSqkmpJXx3JdYlr3ZskaMiHDFY21ByBC+mVpzsEREQREQEREBERARcuHSpJ+qs/Ed+VSN6Un/qrPxD+VFyumoubN6T3fqrfxD+RTs6SSf6sPxD+VDK6Ei0NvSGf1cfifuqZm/t/wCg/wBT91DK3ZfLm3Kc/wAI1MdZiMdJJJPNiN3Tvc4dWTbzSHsDRwbfiSu5jfj/AMH+f91advhFFWyioEXVzYWtdch8cgjeJI8QsDcEW1zBsdBYslWlPUSENLmWcRk3g0WFhbQfJWcm8YBks7sQBz5pci1uto4h5zybC+i1re/eCsY0x9XgYfLkZc4hyv5oWtu2o39CfDcYpJGusNcs8/Wi6p3g2u6rlbO4BpfHoCTYCSQNFzmTa1zxVgCo3eRH4OHscT814HKMrgFVBygxr0OQXDXKrrFbh68LlBctkUrZlZByrBQX8cqmD1jo3KdsiKvmyKrErIPVYkUwXV1QXZqMSLxr81hqNjrdryxU1K6mlewwSSNeGuIGOQiWMlvEFoI/ZK3zYe+baqIPbH9u2wkDbAtJyD7cWk8VySaQWnBOb3QWH9xjgfcferfZ+1XUsrZY3AObqDo5vFpHJblV0+taaqVjZcN2StabZNfBI9natpiY8McD3Hmu4Lj+7VYyokinMEkUQIc7E2xdbMNYDmRficrc10R+9UA4SfdH1W2fpnEWvO3xpxwl+6PqojvvTcpfuj8yM5WzItXO/dLym+6PzKk7/UvKb7rfzIZW1ItUG/1Lym+4PzL3/r6l5TfcH5kMrakWqjf6k5TfcHyKIZXFLqsFU4FWEaSsU7HWUEYJ0BVzHS8yipo5FeQv9ijp4Gjv8VkoXhBGyQKXF3q5bhIsQLeCgfQ64XeAP1QW1XA14s4ArVNq7qQvucABPFuR92RWzS3bqCFA+UIOWbwbN/RjGy5Is4i+uZ96xWJbV0iEF8J/su+IWpXRipA5VY1Dde3UxEokVQkUF0umKuWvVYerPEvcSmC9Eila9Y/Gq2yorIB6rD1YNmVYmRV91iNk48la9YpqAY5GMPnva37zgPmsq6NB0YsJLp6qQh5L8MbWsAxZ2xHFfloFntmbp0dNnFC0vHnv7b/EF2nqssvJPmrd83eumCZ8tlZyyqOaZWkk3cipZJFC53goS9eBBU5vgqCPBVgKklB4GJhCL0IIywdy9VZREYVtPfUqdkIGin6nNC0Ko8apWqK6qCKuYirtjrcVj2lSiQDUoMnHIp2ShYQ17Bxv4ZqGTa/JvtUGxukByNiORWtVsjcbsGTQbD1KCXash42HcPmrbr75m9+aDC71bO64McXWLLjS4INvotLraYxuw68QdLrodWcQWDr9ntk8q4I0IPyRmxqNkWVqNjPb5JDh7D7CrGSEtNnAg94spqYgsvbFShq9smr5Q2KWKnDVVhTV8raxXtirkNXuBTTytbFe4SrsN9qzGzt255c8Ijbzky9jdT7k08xroLguibv7pRAwzuklJAZLgIDbOycAeOR+CUW6UDLGQuld39ln3R8yVn4rNAa0ANbkA3IAcgFcXGadMoXTKwEypM3eqLiSRW73Kl0w4qgzt5FBIxV3Vv1o5rwv71Rd4lTdWzZVI2VBIqg5RYl7dBMUUYkRB7IFbkoiiI3FW76ojQIioodUOPH2KMlEUHhcqcRREFOJeXRFFQyFWsiIiISFTIwEWIBHeiIixm2Ww5js+GixckOE2RFFinCvMKIiqsKzmxNgtmGN7yG+iAAfvf8ACIg2eioYof5ONoI87Vx9ZV5juiLQOddeYl4iDwvXjXoiooe9UYyiIPC5Ukr1EFPXFVx1BRFBO2cqUyIioifOV6iKD//Z"

// dom elements
const DdElement = document.getElementById("laptops");
const featuresElement = document.getElementById("features");
const LsTitleElement = document.getElementById("lower-laptop-title");
const LsInfoElement = document.getElementById("lower-laptop-info");
const LsPriceElement = document.getElementById("lower-laptop-price");
const LsImageElement = document.getElementById("pic");
const workButtonElement = document.getElementById("work-button");
const workDisplayElement = document.getElementById("current-work-money");
const toBankButtonElement = document.getElementById("money-2-bank");
const bankBalanceElement = document.getElementById("current-balance");
const loanButtonElement = document.getElementById("loan-button");
const currentLoanElement = document.getElementById("current-loan");
const repayLoanButtonElement = document.getElementById("repay-loan");
const buyLaptopElement = document.getElementById("buy-laptop");
const currentLoanTitleElement = document.getElementById("loan-title");
const titleFeaturesElement = document.getElementById("features-title");
const framePictureElement = document.getElementById("laptop-picture")


// initially hide some elements
hide(repayLoanButtonElement);
hide(currentLoanTitleElement);
hide(titleFeaturesElement);
hide(framePictureElement);
hide(buyLaptopElement);



// user variables
var workMoney = 0;
var bankMoney = 0;
var loan = 0;
var laptopMoney = 0;
var boughtLaptops = [];
var laptops = await getAsync();

// get initial values on the app
updateBank();
updateWork();

// event listeners
function onSelectChange() {
    const laptopId = parseInt(this.value);
    const pertLaptop = laptops.find(laptop => laptop.id === laptopId);
    renderSelectedLaptop(pertLaptop);
}

DdElement.addEventListener("change", onSelectChange);
workButtonElement.addEventListener("click", work);
toBankButtonElement.addEventListener("click", transferToBank);
loanButtonElement.addEventListener("click", requestLoan);
repayLoanButtonElement.addEventListener("click", repayLoan);
buyLaptopElement.addEventListener("click", buyLaptop);
LsImageElement.addEventListener("error", () => {
    LsImageElement.src = BACK_UP_IMAGE;
})

// get laptops and render in DD menu
async function getAsync() {
    try {
        const response = await fetch(
            "https://noroff-komputer-store-api.herokuapp.com/computers"
        );
        const data = await response.json();
        return data;
    } catch (error) {
        console.error("ERROR:", error);
    }
}

for (const laptop of laptops) {
    DdElement.innerHTML += 
        `<option value=${laptop.id}>${laptop.title}</option>`;
}

// functions dependent on user input

// render the selected laptop 
const renderSelectedLaptop = (laptop) => {
    featuresElement.innerHTML = '';
    for (const feature of laptop.specs) {
        featuresElement.innerHTML += 
            `<li>${feature}</li>`
    }
    LsTitleElement.innerText = laptop.title;
    LsInfoElement.innerText = laptop.description;
    LsPriceElement.innerText = formatToValuta(laptop.price);
    laptopMoney = laptop.price;
    LsImageElement.src = IMAGE_URL + laptop.image;
    LsImageElement.alt = laptop.title;
    show(framePictureElement);
    show(buyLaptopElement);
    show(titleFeaturesElement);
}

// work to add money
function work() {
    workMoney += 100;
    updateWork();
}

// transfer money from work to bank
function transferToBank() {
    if (loan) {
        const due = (workMoney / 10);
        loan = loan - due;
        if (loan <= 0) {
            paidOffLoan();
        }
        updateLoan();
        workMoney = workMoney - due;
    }
    bankMoney += workMoney;
    workMoney = 0;
    updateBank();
    updateWork();
}

// handle request for loan
function requestLoan() {
    var askedLoan = prompt("How much do you want to loan?");
    if (isNaN(askedLoan)) {
        alert("please pass in a number") 
    } else if (askedLoan < 1){
        alert("cannot loan number smaller than 1 (and only whole numbers)")
    } else {
        askedLoan = parseInt(askedLoan);

    if (askedLoan <= (bankMoney*2)) {
        acceptLoan(askedLoan);
    } else {
        alert("you don't have enough money. get to work!")
    }
}}

// change values and functionality according on the accepted loan
function acceptLoan(askedLoan) {
    loan = askedLoan;
    alert(`took out a loan of ${loan} euros`);
    bankMoney += loan;
    updateBank();
    updateLoan();
    loanButtonElement.disabled = true;
}

// change values and functionality because loan is paid of
function paidOffLoan() {
    bankMoney = bankMoney - loan;
    loan = 0;
    alert("great! you paid of your loan.");
    loanButtonElement.disabled = false;
    updateWork();
    updateLoan();
    updateBank();
}

// handle request to pay of loan
function repayLoan() {
    loan = loan - workMoney;
    if (loan <= 0){
        workMoney = 0;
        paidOffLoan()
    } else {
        workMoney = 0;
        updateWork();
        updateLoan();
    }
}

// let bank represent current value in valuta
function updateBank() {
    bankBalanceElement.innerText = formatToValuta(bankMoney);
}

// let work represent current value in valuta
function updateWork() {
    workDisplayElement.innerText = formatToValuta(workMoney);
}

// let loan represent current value and change visibility accordingly
function updateLoan() {
    if (loan === 0) {
        hide(repayLoanButtonElement);
        hide(currentLoanElement)
        hide(currentLoanTitleElement)
    } else{
        show(repayLoanButtonElement);
        show(currentLoanTitleElement);
        show(currentLoanElement);
    }
    currentLoanElement.innerText = formatToValuta(loan);
}

// handle request to buy laptop
function buyLaptop() {
    const boughtLaptop = LsTitleElement.innerText;

    if (laptopMoney <= bankMoney && !(boughtLaptops.includes(boughtLaptop))) {
        alert(`aahyeah!! congratulations with your purchase of the ${boughtLaptop}.
                hope to see you again soon!`);
        boughtLaptops.push(boughtLaptop);
        bankMoney = bankMoney - laptopMoney;
        updateBank();
    } else if (boughtLaptops.includes(boughtLaptop)) {
        alert("you already have this one... maybe buy another model?")
    } else {
        alert("not enough moneys! u need to obtain more coin")
    }
}

// show html element
function show(element) {
    element.style.display = "block";
}

// hide html element
function hide(element) {
    element.style.display = "none";
}

// format number to valuta
function formatToValuta(money) {
    return new Intl.NumberFormat("de-DE",
    {style: 'currency', currency: "EUR"}).format(money);
}